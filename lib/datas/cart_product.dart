import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:loja_virtual/datas/product_data.dart';

class CartProduct {

  //Categoria id
  String cid;
  //Para saber qual é o produto precisamos entrar na categoria
  String category;

  //Produto id
  String pid;

  //Quantidade
  int quantity;
  //Tamanho
  String size;

  //Para armazenar os dados do produto
  //Não iremos armazenar esses dados no banco permanentemente
  ProductData productData;


  CartProduct();


  CartProduct.fromDocument(DocumentSnapshot document){
    cid = document.documentID;
    category = document.data["category"];
    pid = document.data["pid"];
    quantity = document.data["quantity"];
    size = document.data["size"];
  }

  Map<String, dynamic> toMap(){
    return{
      "category" : category,
      "pid" : pid,
      "quantity": quantity,
      "size" : size,
      "product" : productData.toResumedMap()
    };
  }

}