import 'package:flutter/material.dart';
import 'package:loja_virtual/models/cart_model.dart';
import 'package:loja_virtual/models/user_model.dart';
import 'package:loja_virtual/screens/home_screen.dart';
import 'package:scoped_model/scoped_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Scoped Model armazena os estados e as funções capazes de alterar esses estados
    return ScopedModel<UserModel>(
      model: UserModel(),
      //Tudo abaixo do scoped model terá acesso ao UserModel e será modificado caso alguma coisa aconteça no UserModel
      //CartModel fica dentro do scopedModel de usuario porque precisa ter acesso ao usuário
      child: ScopedModelDescendant<UserModel>(
        //CartModel recebe o model do usuário e refaz o app se o usuário for trocado
          builder: (context, child, model){
            return ScopedModel<CartModel>(
              model: CartModel(model),
              child: MaterialApp(
                title: "Flutter's Clothing",
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                  primaryColor: Color.fromARGB(255, 4, 125, 141),
                ),
                debugShowCheckedModeBanner: false,
                home: HomeScreen(),
              ),
            );
          }
      ),
    );
  }
}



