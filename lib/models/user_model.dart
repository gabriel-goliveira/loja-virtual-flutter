import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';


//Model é um objeto que armazena os estados e as funções capazes de alterar esses estados
//Para acessar esse modelo de qualquer lugar do app, devemos adicionar o widget scoped model no main
class UserModel extends Model{


  //FirebaseAuth.instance é um Singleton(só tem uma instância no app) e ficará armazenado no objeto auth
  //para não precisar escrever FirebaseAuth.instance toda vez
  FirebaseAuth _auth = FirebaseAuth.instance;

  //Usuário Logado no momento, se não houver usuário logado no momento, objeto armazena null. Se houver, armazena id e infos
  FirebaseUser firebaseUser;

  //Esse mapa irá abrigar os dados mais importantes do usuário
  Map<String, dynamic> userData = Map();


  //Vai indicar quando nosso UserModel está processando alguma coisa
  bool isLoading = false;

  //Método para acessar o model
  static UserModel of(BuildContext context) =>
      ScopedModel.of<UserModel>(context);


  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
    //Para pegar o usuario atual quando o app inicia
    _loadCurrentUser();
  }

  //Notação required para apontar que é necessário
  void signUp({@required Map<String, dynamic> userData, @required String pass, @required VoidCallback onSuccess, @required VoidCallback onFail}){

    //Notificamos que estamos carregando
    isLoading = true;
    notifyListeners();

    //Tentando criar Usuário
    _auth.createUserWithEmailAndPassword(
        email: userData["email"],

        //Se Funcionar vai executar uma função anônima que recebe o usuario
        password: pass).then((user) async {

          //FirebaseUser recebe o usuário passado na função
          firebaseUser = user;

          //Método para salvar o restante das informações do usuário(endereço, nome e etc) no firestore
          await _saveUserData(userData);

          //Chamar função de sucesso
          onSuccess();

          //Notificar que parou de carregar
          isLoading = false;
          notifyListeners();

     //Se não funcionar
    }).catchError((e){
      //Chamar função de falha
      onFail();
      //Notificar que parou de carregar
      isLoading = false;
      notifyListeners();

    });

  }

  void signIn({@required String email, @required String pass, @required VoidCallback onSuccess, @required VoidCallback onFail}) async{
    isLoading = true;
    notifyListeners();

    //Função sign do firebase recebendo o email e o password e depois executando uma função anonima que recebe user
    _auth.signInWithEmailAndPassword(email: email, password: pass).then((user) async{
      //Se der certo vai executar isso
      //Firebase recebe o user
      firebaseUser = user;

      //Chama a função para verificar o usuario atual
      await _loadCurrentUser();

      //função onSuccess
      onSuccess();
      isLoading = false;
      notifyListeners();


    //Senão
    }).catchError((e){
      onFail();
      isLoading = false;
      notifyListeners();

    });



  }

  void signOut() async{
    await _auth.signOut();
    //Mapa vazio
    userData = Map();
    //firebaseUser fica nulo
    firebaseUser = null;

    //notifica todos
    notifyListeners();

  }

  void recoverPass(String email){
    _auth.sendPasswordResetEmail(email: email);

  }


  //Função para verificar se o usuário está logado
  bool isLoggedIn(){
    //Se o firebaseUser for diferente de nulo, ele está logado
    return firebaseUser != null;
  }



  //Funções que funcionaram só internamente utilizam o underline para não poderem ser chamadas de fora
  Future<Null> _saveUserData(Map<String, dynamic> userData) async {

    //Passando o userData do método para o userData da classe
    this.userData = userData;
    //Cria uma coleção user, cria um documento com o id do Usuário e salva as informações que estavam dentro do objeto userData
    await Firestore.instance.collection("users").document(firebaseUser.uid).setData(userData);
  }

  Future<Null> _loadCurrentUser() async{
    //Tentando pegar o usuário atual caso não tenha usuário logado
    if(firebaseUser == null)
      firebaseUser = await _auth.currentUser();
    //Caso tenha usuário logado
    if(firebaseUser!=null){
      //e o nome do usuário for nulo, pois quando é efetuado o login ele não traz as informações
      if(userData["name"]==null){
        //Traz as informações e armazena no docUser
        DocumentSnapshot docUser = await Firestore.instance.collection("users").document(firebaseUser.uid).get();
        //Passa o mapa para o userData
        userData = docUser.data;
      }
    }
    //Notifica que há os dados do usuario
    notifyListeners();
  }

}