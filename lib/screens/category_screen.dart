import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:loja_virtual/datas/product_data.dart';
import 'package:loja_virtual/tiles/product_tile.dart';

class CategoryScreen extends StatelessWidget {

  final DocumentSnapshot snapshot;
  CategoryScreen(this.snapshot);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromARGB(255, 211, 118, 130),
            title: Text(
                snapshot.data["title"]
            ),
            centerTitle: true,
            bottom: TabBar(
              indicatorColor: Colors.white,
                tabs: <Widget>[
                  Tab(icon: Icon(Icons.grid_on),
                  ),
                  Tab(icon: Icon(Icons.list),
                  ),
                ],
            ),
          ),
          //DocumentSnapshot é a foto de um documento só
          //QuerySnapshot é a foto de uma coleção de documentos
          body: FutureBuilder<QuerySnapshot>(
            //Todos os documentos do itens
            future: Firestore.instance.collection("products").document(snapshot.documentID).collection("itens").getDocuments(),
              builder: (context, snapshot){
                if(!snapshot.hasData){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }else{
                  return TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        //tab de grade, quando iciado chama o widget productTile(lista de produtos) e passa o tipo de widget(grade) por parametro
                       GridView.builder(
                         padding: EdgeInsets.all(4.0),
                           gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                               crossAxisCount: 2,
                             mainAxisSpacing: 4.0,
                             crossAxisSpacing: 4.0,
                             childAspectRatio: 0.65,
                           ),
                           itemCount: snapshot.data.documents.length,
                           //Pega os documentos, cada documento é um produto da categoria
                           //pega cada um desses documentos  através de um index
                           //transforma esse document em um product data e passa ele para o tile
                           itemBuilder: (context, index){
                           ProductData data =  ProductData.fromDocument(snapshot.data.documents[index]);
                           data.category = this.snapshot.documentID;
                            return ProductTile("grid", data);
                           }
                       ),

                        //tab de Lista, quando iciado chama o widget productTile(lista de produtos) e passa o tipo de widget(list) por parametro
                        ListView.builder(
                          padding: EdgeInsets.all(4.0),
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (context, index){
                            ProductData data =  ProductData.fromDocument(snapshot.data.documents[index]);
                            data.category = this.snapshot.documentID;
                            return ProductTile("list", data);
                          }
                        ),
                      ]
                  );
                }
              }
          ),
        ),
    );
  }
}
