import 'package:flutter/material.dart';
import 'package:loja_virtual/datas/cart_product.dart';
import 'package:loja_virtual/datas/product_data.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:loja_virtual/models/user_model.dart';
import 'package:loja_virtual/screens/login_screen.dart';

class ProductScreen extends StatefulWidget {


//Recebe o produto da página da category screen(product tile)
  final ProductData product;
  ProductScreen(this.product);

  @override
  //Passa pro estado
  _ProductScreenState createState() => _ProductScreenState(product);
}

class _ProductScreenState extends State<ProductScreen> {

  final ProductData product;
  //Estado está recebendo o produto e salvando na variavel acima
  _ProductScreenState(this.product);

  String size;

  @override
  Widget build(BuildContext context) {

    final Color primaria = Theme.of(context).primaryColor;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 211, 118, 130),
        title: Text(
            product.title,
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          //Largura dividida por altura
          AspectRatio(
              aspectRatio: 0.9,
            child: Carousel(
              //transforma as imagens em mapa e chama uma função anonima passando url como parametro
              //product.image é uma array de url, tem links, textos e etc
              images: product.images.map((url){
                //chama a network image com a url
                return NetworkImage(url);
              }).toList(),
              dotSize: 5.0,
              dotSpacing: 15.0,
              dotBgColor: Colors.transparent,
              dotColor: Color.fromARGB(255, 211, 118, 130),
              autoplay: false,
              animationCurve: Curves.easeIn,
            ),
          ),
          Padding(
              padding: EdgeInsets.all(16.0),
            child: Column(
              //Strech para tudo ficar esticado, ocupar a largura maxima
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  product.title,
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                  ),
                  //Para não ocupar mais de 3 linhas
                  maxLines: 3,
                ),
                Text(
                  //Duas casas decimais
                  "R\$ ${product.price.toStringAsFixed(2)}",
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    color: primaria,
                  ),
                ),
                SizedBox(height: 16.0),
                Text(
                  "Tamanhos",
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(height: 34.0,
                child: GridView(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                  scrollDirection: Axis.horizontal,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      //Uma linha só
                        crossAxisCount: 1,
                      //Espaçamento horizontal
                      mainAxisSpacing: 8.0,
                      childAspectRatio: 0.5,
                    ),
                  //Mapeando e transformando em lista
                  children: product.sizes.map(
                      (sizes){
                        return GestureDetector(
                          onTap: (){
                            setState(() {
                              //Variavel tamanho receberá o tamanho que foi clicado
                              size = sizes;
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(4.0)),
                              border: Border.all(
                                // Se algum dos tamanhos for igual ao tamanho clicado, a cor mudará
                                color: sizes == size ? primaria : Colors.grey[500],
                                width: 3.0,
                              ),
                            ),
                            width: 50.0,
                            alignment: Alignment.center,
                            child: Text(sizes),
                          ),
                        );
                      }
                  ).toList(),
                ),
                ),
                SizedBox(height: 16.0,),
                SizedBox(
                  height: 44.0,
                  child: RaisedButton(
                      onPressed:
                        //Se size for diferente de nulo(se for selecionado) ele executará uma função, caso contrário
                        //ele não retorna nenhuma função, o que torna o botão desabilitado
                        size != null ?
                        (){
                          if(UserModel.of(context).isLoggedIn()){

                            CartProduct cartProduct = CartProduct();
                            cartProduct.size = size;
                            cartProduct.quantity =1;
                            cartProduct.pid = product.id;
                            cartProduct.category = product.category;

                            
                          }else{
                            Navigator.of(context).push(MaterialPageRoute(builder: (context)=> LoginScreen()));
                          }
                        } :  null,


                    child: Text( UserModel.of(context).isLoggedIn() ?
                      "ADICIONAR AO CARRINHO" : "ENTRE PARA COMPRAR",
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                    ),
                    textColor: Colors.white,
                    color: primaria,
                  ),
                ),
                SizedBox(height: 16.0,),
              Text(
                "Descrição",
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
                Text(
                  product.description,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
