import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:transparent_image/transparent_image.dart';

class HomeTab extends StatelessWidget {



  @override
  Widget build(BuildContext context) {

    Widget _buildBodyBack() => Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 211, 118, 130),
              Color.fromARGB(255, 253, 181, 168)
            ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
    );

    return Stack(//Quando vc quer colocar uma coisa em cima da outra, utilize o stack
      children: <Widget>[
        _buildBodyBack(),
        CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              floating: true,
              snap: true, //Se descer a tela a barra some e se subir ela aparece
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              flexibleSpace: FlexibleSpaceBar(
                title: const Text("Novidades"),
                centerTitle: true,
              ),
            ),
            FutureBuilder<QuerySnapshot>(
              //puxa os resultados
              future: Firestore.instance.collection("home").orderBy("posicao").getDocuments(),
              builder: (context, snapshot){
                //Se não houver dados, aparece um simbolo de carregando
                if(!snapshot.hasData){
                  return SliverToBoxAdapter(
                    child: Container(
                      height: 200.0,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      ),
                    ),
                  );
                } else{
                  //Senão, aparecerá a grid com as imagens
                  return SliverStaggeredGrid.count(
                      crossAxisCount: 2,
                    mainAxisSpacing: 1.0,
                    crossAxisSpacing: 1.0,
                    //transforma em mapa e chama uma função anônima
                    staggeredTiles: snapshot.data.documents.map(
                        (doc){
                          //Pega o tamanho das imagens
                          return StaggeredTile.count(doc.data["x"], doc.data["y"]);//extensão/tamanho das imagens
                        }
                    ).toList(),
                    children: snapshot.data.documents.map(
                            (doc){
                          return FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            //pega a imagem
                            image: doc.data["image"],
                            fit: BoxFit.cover,
                          );
                        }
                    ).toList(),
                  );
                }
              },
            ),
          ],
        ),
      ],
    );
  }
}
