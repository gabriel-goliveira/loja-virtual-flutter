import 'package:flutter/material.dart';

class DrawerTile extends StatelessWidget {

  final IconData icon;
  final String text;
  final PageController controller;
  final int page;

  //Contrutor que vai receber um ícone, o titulo, o controller enviado da homescreen para a drawer e a drawer constrói a tile
  //passando o mesmo controller para a troca de páginas
  //Passa a pagina de cada tab
  DrawerTile(this.icon, this.text, this.controller, this.page);


  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: (){
          Navigator.of(context).pop();
          controller.jumpToPage(page);
        },
        child: Container(
          height: 60.0,
          child: Row(
            children: <Widget>[
              Icon(
                icon,
                size: 32.0,
                //Se for a página atual, cor principal senão cinza
                //o controller pega a página atual que da um numero do tipo double aproximado e o método round arredonda
                color: controller.page.round() == page ?
                Theme.of(context).primaryColor : Colors.grey[700],
              ),
              SizedBox(
                width: 32.0,
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 16.0,
                  //Se for a página atual, cor principal senão cinza
                  //o controller pega a página atual que da um numero do tipo double aproximado e o método round arredonda
                  color: controller.page.round() == page ?
                  Theme.of(context).primaryColor : Colors.grey[700],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
