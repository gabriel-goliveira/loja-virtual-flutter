import 'package:flutter/material.dart';
import 'package:loja_virtual/datas/product_data.dart';
import 'package:loja_virtual/screens/product_screen.dart';

class ProductTile extends StatelessWidget {

  final String type;
  final ProductData product;
  ProductTile(this.type, this.product);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        //Chama a tela do produto, passando o produto que foi clicado
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProductScreen(product)));
      },
      child: Card(
        //Se for grid adiciona uma coluna, senao adiciona uma linha
        child: type == "grid" ?
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                //Para imagem nao variar em cada dispositivo
                //Para uma imagem quadrada utilizar 1.0
                AspectRatio(
                    aspectRatio: 0.8,
                  child: Image.network(
                    product.images[0],
                    fit: BoxFit.cover,
                  ),
                ),
                //pega o espaco disponivel
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.start, - Para alinhar a esquerda
                      children: <Widget>[
                        Text(product.title,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                        ),
                        ),
                        Text(
                          "R\$ ${product.price.toStringAsFixed(2)}" ,
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),

                ),
              ],
            ) :

            //lista
            Row(
              children: <Widget>[
                Flexible(
                    child: Image.network(
                      product.images[0],
                      fit: BoxFit.cover,
                      height: 250.0,
                    ),
                  flex: 1,
                ),
                Flexible(
                    child:  Container(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(product.title,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            "R\$ ${product.price.toStringAsFixed(2)}" ,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  flex: 1,
                ),
              ],
            ),
      ),
    );
  }
}
