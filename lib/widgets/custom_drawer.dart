import 'package:flutter/material.dart';
import 'package:loja_virtual/models/user_model.dart';
import 'package:loja_virtual/screens/login_screen.dart';
import 'package:loja_virtual/tiles/drawer_tile.dart';
import 'package:scoped_model/scoped_model.dart';

class CustomDrawer extends StatelessWidget {

  //Controller
  final PageController pageController;
  //Construtor que recebe o Pagecontroller que está na homescreen
  CustomDrawer(this.pageController);

  @override
  Widget build(BuildContext context) {
    //Degrade do fundo da drawer
    Widget _drawerBodyBack() => Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromARGB(255, 203, 236, 241),
            Colors.white,
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
    );

    return Drawer(
      child: Stack(
        children: <Widget>[
          _drawerBodyBack(),
          ListView(
            padding: EdgeInsets.only(left: 30.0, top: 10.0),
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 8.0),
                padding: EdgeInsets.fromLTRB(0.0, 16.0, 16.0, 8.0),
                height: 170.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 15.0,
                        left: 0.0,
                        child: Text(
                          "Flutter's\nClothing",
                          style: TextStyle(
                            fontSize: 34.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                    ),
                    Positioned(
                      bottom: 0.0,
                        left: 0.0,
                        child: ScopedModelDescendant<UserModel>(
                          builder: (context, child, model){
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  //Verificar se está logado, se estiver aparece o nome do usuario
                                  "Olá, ${!model.isLoggedIn() ? "": model.userData["name"]}",
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                GestureDetector(
                                  onTap: (){
                                    if(!model.isLoggedIn()) {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context) =>
                                            LoginScreen()),
                                      );
                                    }else{
                                      model.signOut();
                                    }
                                  },
                                  child: Text(
                                    //Verificar se está logado, se estiver aparece determinado texto senao aparece outro
                                    !model.isLoggedIn() ?
                                    "Entre ou cadastre-se >" :
                                    "Sair",
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                    ),
                  ],
                ),
              ),
              Divider( ),
                //DrawerTile é um widget criado para gerar o menu da drawer
              //Esse Widget recebe um icone, um titulo, o pagecontroller para mudar de página com o Pageview que é passado na homescreen
              //e o número correspondente da página dentro da PageView
                DrawerTile(Icons.home, "Início", pageController, 0),
                DrawerTile(Icons.list, "Produtos", pageController, 1),
                DrawerTile(Icons.location_on, "Lojas", pageController, 2),
                DrawerTile(Icons.playlist_add_check, "Meus Pedidos", pageController, 3),
            ],
          ),
        ],
      ),
    );
  }
}
